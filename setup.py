"""Installation and testing configuration."""

from os import path
import sys

from setuptools import setup

try:
    import asyncio  # pylint: disable=unused-import
except ImportError:
    sys.exit('arpaio requires asyncio')

HERE = path.abspath(path.dirname(__file__))
with open('{}/README.md'.format(HERE)) as readme:
    LONG_DESCRIPTION = readme.read()

setup(
    author='Jonathan Sharpe',
    author_email='mail@jonrshar.pe',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Embedded Systems',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    description='Asynchronous Raspberry Pi I/O.',
    license='ISC',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    name='arpaio',
    packages=['arpaio'],
    test_suite='tests',
    url='https://gitlab.com/textbook/arpaio',
    version='0.0.1',
)
